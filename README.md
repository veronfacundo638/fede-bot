# Fede-Bot

-Es un proyecto pensado en generar una comunidad colaborativa, de quinto para quinto. Pensando en sanar las consultas reiterativas a fede y seba, en su mayoria, pero tambien generando un base de datos rica en tecnica sobre tecnologías

## Getting started

-Por el momento, esto es un MVP, lo que con lleva crear, lanzar, y modificar rapido. Por esto mismo, vamos a empezar con 4 areas. Scrum, Front, Back, Infrestructura. Por lo tanto, al buscar y al generar es super importante disernir a que area pertence. Por ahi hay tutoriales multi-area, lo aconsejable, es dividir ese tutorial segun el area y anexarlo en extras, con un link a el otro repositorio

## Hay 2 tipos de archivos que vamos a crear:

### 1. Descarga, Instalacion, Actualizacion, de software
### 2. Ejemplo de codigos

-Por el momento no existe una diferencia marcada entre ambos archivos, pero es necesario diferenciar los archivos a fin de mejorar las busquedas, y la enseñanza. 

-Trantando de usar el primer principio SOLID. Responsibilidad única, si vas a hacer una tutorial donde creas una api-rest, la idea sea separar la instalacion de alguna dependencia, con la de crear codigo. Y la relacionamos con el apartado de conlusion o pre-requisistos, dependiendo del orden del tutorial ,pegando el link de los repositorios asociados 

## Creacion de archivos

-Para la creacion de archivos tenemos una plantilla generica, donde la idea seria poder respetar esta estructura pre-definida. 

-Los archivos deben tener la extension .md, para poder aplicar los estilos

## Estilos usados

-Para titulos basta usar # antes de escribir el titulo ej: # Titulo
-Siguiendo los titulos, la cantidad de # que usemos jerarquizas los titulos, analogamente seria la etiqueta h1 en html 

-Para exponer codigo o comandos usamos  ``` y ponemos esto antes y despues del codigo o instruccion, para diferenciar

-Para hacer un link, usamos [](), [] para agregar el link simbolico, y () para agregar el link real. Se usa asi []()

-De igual manera, para ver de forma de codigo la plantilla, basta con ver la platilla en forma de codigo.

## Conclusion

-A manera de saludo y de reflexion, los invito con mucho entusiasmo a participar de este proposito, es de quinto, para quinto
